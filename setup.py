import pathlib
from setuptools import setup, find_packages


HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()


def read_requirements():
    """Parses requirements from requirements.txt"""
    reqs_path = HERE / "requirements.txt"
    with open(reqs_path, encoding="utf8") as f:
        reqs = [line.strip() for line in f if not line.strip().startswith("#")]
    return reqs


setup(
    name="DMOZ Dataset Project",
    version="0.1.0",
    description="Recreation of the DMOZ 2006 Dataset in a feather file.",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/michal.hybinski/dmoz_dataset.git",
    author="Michał Hybiński",
    author_email="michal.hybinski@gmail.com",
    python_requires=">=3.9.15",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
        "Topic :: Scientific/Engineering",
    ],
    packages=find_packages(exclude=["tests", "tests.*"]),
    package_data={},
    include_package_data=True,
    install_requires=read_requirements(),
)
