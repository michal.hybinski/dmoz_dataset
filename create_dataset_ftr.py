import pandas as pd
from pathlib import Path
import shutil
import os
import tarfile
from tqdm import tqdm
import argparse


if __name__ == "__main__":

    # Parse the arguments
    parser = argparse.ArgumentParser(description="Include Wikification, Leave temporary files")

    parser.add_argument("-wiki", "--wikification",
                        action='store_true',
                        required=False,
                        help="Decide whether include the Wikification in the ftr file.")

    parser.add_argument("-ltf", "--leave_temp_files",
                        action='store_true',
                        required=False,
                        help="Prevents temporary files from being removed at the end of the process.")

    args = parser.parse_args()

    # Unpack tar.gz files
    # List of dataset elements to unpack
    list_of_elements = ['urls', 'texts']
    if args.wikification:
        list_of_elements.append('wikified')
    else:
        pass

    # Files extraction
    for element in list_of_elements:
        with tarfile.open(f"data/{element}.tar.gz", "r:gz") as tar:
            for member in tqdm(iterable=tar.getmembers(), total=len(tar.getmembers()), desc=f'Unpacking {element}'):
                tar.extract(member=member, path='./data/')
    print('Unpacking completed.')

    # Create DataFrame with Topics
    df_topics = pd.read_csv('data/topic_names.txt', sep='\t', header=None)
    df_topics.columns = ['topic_index', 'topic_name']
    df_topics.set_index('topic_index', inplace=True)

    # Create DataFrames for all Topics
    for topic_index in tqdm(iterable=df_topics.index, total=len(df_topics.index),
                            desc='Creating ftr files for every topic_index'):

        # DataFrame with all URLs for topic_index:
        df_url = pd.DataFrame()
        if Path(f'data/urls/{topic_index}.txt').is_file():
            df_url['url'] = pd.read_csv(f'data/urls/{topic_index}.txt', sep='\t', header=None)

            # DataFrame with text (and optionally wikification) for url:
            for element in list_of_elements[1:]:
                temp_dict = dict()
                temp_folder = Path(f'data/{element}/{topic_index}')
                temp_files = os.listdir(temp_folder)

                for file_name in temp_files:
                    with open(Path(temp_folder, file_name), 'r', encoding='utf-8') as f:
                        try:
                            temp_dict[int(Path(file_name).stem)] = f.read()
                        except ValueError:
                            continue

                # create df_text:
                df_temp = pd.DataFrame(temp_dict.items(),
                                       index=temp_dict.keys(),
                                       columns=['index', f'{element}'])
                df_temp.drop('index', axis=1, inplace=True)

                # join DataFrames:
                df_url = df_url.join(df_temp)

        # handle empty topic_index
        else:
            df_url['url'] = None
            for element in list_of_elements[1:]:
                df_url[element] = None

        # add columns: topic_index, topic_name:
        df_url['topic_index'] = topic_index
        df_url['topic_name'] = df_topics.loc[topic_index]['topic_name']

        # save feathers: topic_index_{topic_index}.ftr:
        df_url.reset_index(drop=True).to_feather(f'data/dataframes/topic_index_{topic_index}.ftr',
                                                 compression='zstd', compression_level=3)

    # Load ftrs with topics and create a dataframe with the complete dataset
    # list of columns
    columns = ['url', 'texts', 'topic_index', 'topic_name']
    if args.wikification:
        columns.insert(2, 'wikified')
    else:
        pass

    # empty df_dataset with desired columns
    df_dataset = pd.DataFrame()

    # load and concatenate all topic_index ftr files with df_dataset
    for topic_index in tqdm(iterable=df_topics.index, total=len(df_topics.index),
                            desc='Creating the output file'):
        df_new = pd.read_feather(f'data/dataframes/topic_index_{topic_index}.ftr')
        df_dataset = pd.concat([df_dataset, df_new], axis=0, ignore_index=True)

    # adjust columns' names
    df_dataset.rename(columns={'texts': 'text'}, inplace=True)

    # save df_dataset as ftr:
    df_dataset.reset_index(drop=True).to_feather(f'dmoz_dataset.ftr',
                                                 compression='zstd',
                                                 compression_level=6)

    # remove temporary files
    if args.leave_temp_files:
        print('Leaving temporary files.')
    else:
        # removing folders
        print('Preparing to remove temporary files.')
        folders_to_remove = ['data/urls', 'data/texts']
        if args.wikification:
            folders_to_remove.append('data/wikified')

        folders_to_remove = [shutil.rmtree(folder_) for folder_ in folders_to_remove]

        # removing ftr files
        files_to_remove = os.listdir('data/dataframes')
        for file_ in tqdm(iterable=files_to_remove, total=len(files_to_remove),
                          desc='Removing temporary files'):
            if file_ != '__init__.py':
                try:
                    os.remove(f'data/dataframes/{file_}')
                    # print(f"{file_} deleted successfully")
                except OSError as e:
                    print(f"Error deleting {file_}: {e.strerror}")
                    continue

    # result statement
    print('Process finished.',
          f'\nThe output file: {Path("dmoz_dataset.ftr").resolve()}',
          f'\nSize: {os.path.getsize("dmoz_dataset.ftr") / (1024 * 1024): .2f} MB')
