## DMOZ Project

This is a repository to recreate [DMOZ 2006 Dataset and its Wikification](https://data.mendeley.com/datasets/9mpgz8z257/1) in a lightweight feather file.

## Prerequisites

Python >= 3.9.15

## Installation

To install from source:

    git clone https://gitlab.com/michal.hybinski/dmoz_dataset.git
    cd dmoz_dataset  
    pip install -e .  

## Data

Dataset:\
[Lorenzetti, Carlos; Maguitman, Ana; Baggio, Cecilia (2019), \
“DMOZ 2006 Dataset and its Wikification”, \
Mendeley Data, V1, doi: 10.17632/9mpgz8z257.1](https://data.mendeley.com/datasets/9mpgz8z257/1)

Dataset License: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

After the download expected location of unzipped files is "./data/".

## Example Use

Create a feather file with the Dataset (without Wikification)*:

    python -m create_dataset_ftr

Create a feather file with the Dataset including Wikification*:

    python -m create_dataset_ftr --wikification

The output file is: "dmoz_dataset.ftr"

***Note:\
When the dataset is recreated, new folders containing decompressed .tar.gz files and separate .ftr files for each topic are created in the directory './data/'. These files are automatically removed at the end of the process. However, if you want to keep these files and prevent them from being deleted, you can pass the additional argument '--leave_temp_files' to abort the removing operation.\
\
e.g.***

    python -m create_dataset_ftr --leave_temp_files

## License

This project is released under the [MIT License](LICENSE.md).
